#!/usr/bin/perl -w
my $timestamp= "Time-stamp: \"vcard2abook.pl was last updated on Sun, 17 Dec 2000 10:34am\"";

#==============================================================================*
#   vcard2abook.pl by jeff covey <jeff.covey@pobox.com>                        *
#                                                                              *
#   this script has two main features:                                         *
#                                                                              *
#   1. it converts a file containing addressbook entries in vcard format to    *
#      one containing entries in abook format.                                 *
#   2. it almost has more comments than code.                                  *
#                                                                              *
#   This program is free software; you can redistribute it and/or modify       *
#   it under the terms of the GNU General Public License as published by       *
#   the Free Software Foundation; either version 2 of the License, or          *
#   (at your option) any later version.                                        *
#                                                                              *
#   This program is distributed in the hope that it will be useful,            *
#   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
#   GNU General Public License for more details.                               *
#                                                                              *
#   You should have received a copy of the GNU General Public License          *
#   along with this program; if not, write to the Free Software                *
#   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.                  *
#==============================================================================*

use strict;

($#ARGV >= 1) or die
  "usage: vcard2abook.pl <vcard input file> <abook output file>\noutput file will be overwritten!\n";

my $vcards="$ARGV[0]";
my $abook ="$ARGV[1]";
my $key;

my %conversions = (
		   "FN"         => "name=",
		   "NICKNAME"   => "nick=",
		   "EMAIL"      => "email=",
		   "ORG"        => "notes=",
		   "NOTE"       => "notes=",
		   "URL"        => "url=",
		   
		   "TEL;HOME"   => "phone=",
		   "TEL;PREF"   => "phone=",
		   "TEL;VOICE"  => "phone=",
		   "TEL;MSG"    => "phone=",
		   "TEL;VIDEO"  => "phone=",
		   "TEL;MODEM"  => "phone=",
		   "TEL;ISDN"   => "phone=",
		   "TEL;WORK"   => "workphone=",
		   "TEL;CELL"   => "mobile=",
		   "TEL;PAGER"  => "mobile=",
		   "TEL;CAR"    => "mobile=",
		   "TEL;FAX"    => "fax=",
		   );

open (VCARDS,"$vcards") or quit("couldn't open $vcards");
open (ABOOK,">$abook")  or quit("couldn't open $abook for writing");

while (<VCARDS>) {
  if    (/^\s*$/)         { }
  elsif (/^BEGIN:VCARD/i) { print ABOOK "[]\n"; }
  elsif (/^END:VCARD/i)   { print ABOOK   "\n"; }
  else {
    chomp; my @sections=split /:/, $_, 2;
    if ($sections[0] =~ /^ADR/i) {
      my @fields=split /;/, $sections[1];
      if ($fields[2]) {print ABOOK "address=$fields[2]\n";}
      if ($fields[3]) {print ABOOK "city=$fields[3]\n";   }
      if ($fields[4]) {print ABOOK "state=$fields[4]\n";  }
      if ($fields[5]) {print ABOOK "zip=$fields[5]\n";    }
      if ($fields[6]) {print ABOOK "country=$fields[6]\n";}
    }
    else {
      foreach $key (keys %conversions) {
	if ($sections[0] =~ /^$key/i) {
	  print ABOOK "$conversions{$key}$sections[1]\n";
	}
      }
    }
  }
}

close (VCARDS) or quit("couldn't close $vcards");
close (ABOOK)  or quit("couldn't close $abook");

sub quit {
  print "whoops!  $_[0]:\n $!\n"; die;
}
